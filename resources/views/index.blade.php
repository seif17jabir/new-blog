    @extends('layouts.app')

    @section('content')
        <div class="bg-black grid grid-cols-1 m-auto">
            <div class="flex text-gray-100 pt-10">
                <div class="m-auto pt-4 pb-16 sm:m-auto w-4/5 block text-center">
                    <h1 class="sm:text-white text-5xl uppercase font-bold text-shadow-md pb-14">
                    Your best place to write some Blogs
                    </h1>
                </div>
            </div>
        </div>

    @endsection